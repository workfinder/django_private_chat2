from typing import Awaitable, Optional, Set, Tuple

from channels.db import database_sync_to_async
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser
from django.core.exceptions import ValidationError

from django_private_chat2.utils import get_user_id

from ..models import Dialog, Message, UploadedFile, User


@database_sync_to_async
def get_groups_to_add(u: AbstractBaseUser) -> Awaitable[Set[int]]:
    lst = Dialog.get_dialogs_for_user(u)
    return set(list(sum(lst, ())))


@database_sync_to_async
def get_user_by_id(id) -> Awaitable[Optional[AbstractBaseUser]]:
    return User.objects.filter(**{settings.CHAT_USER_ID_FIELD: id}).first()


@database_sync_to_async
def get_file_by_id(file_id: str) -> Awaitable[Optional[UploadedFile]]:
    try:
        f = UploadedFile.objects.filter(id=file_id).first()
    except ValidationError:
        f = None
    return f


@database_sync_to_async
def get_message_by_id(mid: int) -> Awaitable[Optional[Tuple[str, str]]]:
    msg: Optional[Message] = Message.objects.filter(id=mid).first()
    if msg:
        return get_user_id(msg.recipient), get_user_id(msg.sender)
    else:
        return None


@database_sync_to_async
def mark_message_as_read(mid: int) -> Awaitable[None]:
    return Message.objects.filter(id=mid).update(read=True)


@database_sync_to_async
def get_unread_count(sender_id, recipient_id) -> Awaitable[int]:
    return Dialog.get_unread_count_between(sender_id, recipient_id)


@database_sync_to_async
def save_text_message(
    text: str, from_: AbstractBaseUser, to: AbstractBaseUser
) -> Awaitable[Message]:
    message = Message(text=text, sender=from_, recipient=to)
    message.save()
    return message


@database_sync_to_async
def save_file_message(
    file: UploadedFile, from_: AbstractBaseUser, to: AbstractBaseUser
) -> Awaitable[Message]:
    message = Message(file=file, sender=from_, recipient=to)
    message.save()
    return message
