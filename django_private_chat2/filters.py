from django.db.models import Q
from django_filters.filters import CharFilter
from django_filters.filterset import FilterSet

from .conf import settings
from .models import Message


class MessageFilterSet(FilterSet):
    correspondent = CharFilter(method="filter_correspondent", required=True)

    @staticmethod
    def filter_correspondent(qs, name, value):
        return qs.filter(
            Q(**{f"sender__{settings.CHAT_USER_ID_FIELD}": value})
            | Q(**{f"recipient__{settings.CHAT_USER_ID_FIELD}": value})
        )

    class Meta:
        models = Message
        fields = []
