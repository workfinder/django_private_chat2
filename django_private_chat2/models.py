import uuid
from typing import Any, Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from model_utils.models import SoftDeletableModel, TimeStampedModel

from .conf import settings
from .fields import HTMLField

User: AbstractBaseUser = get_user_model()


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return f"user_{instance.uploaded_by.id}/{filename}"


class UploadedFile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    uploaded_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("Uploaded_by"),
        to_field=settings.CHAT_USER_ID_FIELD,
        related_name="+",
        db_index=True,
    )
    file = models.FileField(
        verbose_name=_("File"),
        blank=False,
        null=False,
        upload_to=user_directory_path,
    )
    upload_date = models.DateTimeField(auto_now_add=True, verbose_name=_("Upload date"))

    def __str__(self):
        return str(self.file.name)


class Dialog(TimeStampedModel):
    id = models.BigAutoField(primary_key=True, verbose_name=_("Id"))
    user1 = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("User1"),
        to_field=settings.CHAT_USER_ID_FIELD,
        related_name="+",
        db_index=True,
    )
    user2 = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("User2"),
        to_field=settings.CHAT_USER_ID_FIELD,
        related_name="+",
        db_index=True,
    )

    class Meta:
        unique_together = (("user1", "user2"), ("user2", "user1"))
        verbose_name = _("Dialog")
        verbose_name_plural = _("Dialogs")

    def __str__(self):
        return _("Dialog between ") + f"{self.user1_id}, {self.user2_id}"

    @classmethod
    def get_between(cls, u1: AbstractBaseUser, u2: AbstractBaseUser) -> Optional[Any]:
        try:
            return cls.objects.filter(
                Q(user1=u1, user2=u2) | Q(user1=u2, user2=u1)
            ).first()
        except User.DoesNotExist:
            return None

    @classmethod
    def dialog_exists(cls, u1: AbstractBaseUser, u2: AbstractBaseUser) -> Optional[Any]:
        return cls.get_between(u1, u2) is not None

    @classmethod
    def create_if_not_exists(cls, u1: AbstractBaseUser, u2: AbstractBaseUser):
        res = cls.dialog_exists(u1, u2)
        if not res:
            Dialog.objects.create(user1=u1, user2=u2)

    @classmethod
    def get_dialogs_for_user(cls, user: AbstractBaseUser):
        return cls.objects.filter(Q(user1=user) | Q(user2=user)).values_list(
            "user1_id",
            "user2_id",
        )

    @staticmethod
    def get_messages_between(user1, user2):
        return Message.objects.filter(
            Q(sender=user1, recipient=user2) | Q(sender=user2, recipient=user1)
        )

    @property
    def messages(self):
        return self.get_messages_between(self.user1, self.user2)

    @classmethod
    def get_unread_count_between(cls, recipient, sender) -> int:
        return (
            Message.objects.filter(sender=sender, recipient=recipient)
            .filter(read=False)
            .count()
        )

    @property
    def last_message(self) -> int:
        return self.messages.order_by("-created").first()


class Message(TimeStampedModel, SoftDeletableModel):
    id = models.BigAutoField(primary_key=True, verbose_name=_("Id"))
    sender = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("Author"),
        related_name="from_user",
        to_field=settings.CHAT_USER_ID_FIELD,
        db_index=True,
    )
    recipient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("Recipient"),
        related_name="to_user",
        to_field=settings.CHAT_USER_ID_FIELD,
        db_index=True,
    )
    text = HTMLField(verbose_name=_("Text"), blank=True)
    file = models.ForeignKey(
        UploadedFile,
        related_name="message",
        on_delete=models.DO_NOTHING,
        verbose_name=_("File"),
        blank=True,
        null=True,
    )

    read = models.BooleanField(verbose_name=_("Read"), default=False)
    all_objects = models.Manager()

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        Dialog.create_if_not_exists(self.sender, self.recipient)

    class Meta:
        ordering = ("-created",)
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")


# TODO:
# Possible features - update with pts
# was_online field for User (1to1 model)
# read_at - timestamp
