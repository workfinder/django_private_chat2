from django.contrib.admin import ModelAdmin, site

from .models import Dialog, Message


class MessageAdmin(ModelAdmin):
    readonly_fields = (
        "created",
        "modified",
    )
    search_fields = ("id", "text", "sender__pk", "recipient__pk")
    list_display = ("id", "sender", "recipient", "text", "file", "read")
    list_display_links = ("id",)
    list_filter = ("sender", "recipient")
    date_hierarchy = "created"


class DialogAdmin(ModelAdmin):
    readonly_fields = (
        "created",
        "modified",
    )
    search_fields = ("id", "user1__pk", "user2__pk")
    list_display = ("id", "user1", "user2")
    list_display_links = ("id",)
    date_hierarchy = "created"


site.register(Dialog, DialogAdmin)
site.register(Message, MessageAdmin)
