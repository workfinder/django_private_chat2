from django.db.models import Q
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from .filters import MessageFilterSet
from .models import Dialog, Message
from .serializers import DialogSerializer, MessageSerializer
from .utils import get_user_id


class MessageViewSet(ListModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageSerializer
    filterset_class = MessageFilterSet

    def get_queryset(self):
        return (
            Message.objects.filter(
                Q(
                    recipient=self.request.user,
                )
                | Q(
                    sender=self.request.user,
                )
            )
            .select_related("sender", "recipient")
            .order_by("-created")
        )


class DialogViewSet(RetrieveModelMixin, ListModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = DialogSerializer
    lookup_field_slug = "correspondent_id"

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        correspondent_id = self.kwargs[lookup_url_kwarg]

        return get_object_or_404(
            Dialog.objects.select_related("user1", "user2"),
            Q(user1=self.request.user, user2_id=correspondent_id)
            | Q(user1_id=correspondent_id, user2=self.request.user),
        )

    def get_queryset(self):
        user_id = get_user_id(self.request.user)
        qs = Dialog.objects.filter(
            Q(user1_id=user_id) | Q(user2_id=user_id)
        ).select_related("user1", "user2")
        return qs.order_by("-created")
