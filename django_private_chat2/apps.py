from django.apps import AppConfig


class DjangoPrivateChat2Config(AppConfig):
    name = "django_private_chat2"
