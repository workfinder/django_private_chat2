import os
from typing import Dict

from rest_framework import serializers

from .models import Dialog, Message, UploadedFile, User


def serialize_file_model(m: UploadedFile) -> Dict[str, str]:
    return {
        "id": str(m.id),
        "url": m.file.url,
        "size": m.file.size,
        "name": os.path.basename(m.file.name),
    }


class FileSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    @staticmethod
    def get_url(obj):
        return obj.file.url

    @staticmethod
    def get_size(obj):
        return obj.file.size

    @staticmethod
    def get_name(obj):
        return os.path.basename(obj.file.name)

    class Meta:
        model = UploadedFile
        fields = ("id", "url", "size", "name")


class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.SlugRelatedField(
        queryset=User.objects,
        slug_field="uuid",
        help_text="The UUID of the sending user",
    )
    recipient = serializers.SlugRelatedField(
        queryset=User.objects,
        slug_field="uuid",
        help_text="The UUID of the receivign user",
    )
    file = FileSerializer()
    out = serializers.SerializerMethodField()
    sender_username = serializers.CharField(source="sender.get_username")

    def get_out(self, obj):
        return self.context["request"].user == obj.sender

    class Meta:
        model = Message
        fields = (
            "id",
            "text",
            "created",
            "modified",
            "read",
            "file",
            "sender",
            "sender_username",
            "recipient",
            "out",
        )


class DialogSerializer(serializers.ModelSerializer):
    last_message = MessageSerializer()
    correspondent_id = serializers.SerializerMethodField()
    correspondent_full_name = serializers.SerializerMethodField()
    unread_count = serializers.SerializerMethodField()

    def get_correspondent_id(self, obj):
        return self.get_correspondent(obj).id

    def get_correspondent_full_name(self, obj):
        return self.get_correspondent(obj).full_name

    def get_correspondent(self, obj):
        if self.context["request"].user == obj.user1:
            return obj.user2
        return obj.user1

    def get_out(self, obj):
        return self.context["request"].user == obj.sender

    def get_unread_count(self, obj):
        user = self.context["request"].user
        return Dialog.get_unread_count_between(user, self.get_correspondent(obj))

    class Meta:
        model = Dialog
        fields = (
            "id",
            "created",
            "modified",
            "correspondent_id",
            "correspondent_full_name",
            "unread_count",
            "last_message",
        )


class PolymorphicSerializer(serializers.Serializer):
    """
    A polymorphic serializer that serializes objects and deserializes data
    depending on the value of a discriminator field. Each subtype has its own
    serializer.
    """

    discriminator_field = "type"
    discriminator_map = {
        # Maps the values of the discriminator field to a subserializer.
    }
    class_map = {
        # Maps the possible classes of the instance to
    }

    def __new__(cls, instance=None, data=None, **kwargs):
        discriminator_field = getattr(
            cls.Meta,
            "discriminator_field",
            PolymorphicSerializer.Meta.discriminator_field,
        )
        if instance is not None:
            if hasattr(cls.Meta, "class_map") is not None:
                serializer_class = cls.Meta.class_map[instance.__class__]
            else:
                serializer_class = getattr(instance, cls.Meta.class_map)
        elif data is not None:
            serializer_class = cls.Meta.discriminator_map[data[discriminator_field]]

        return serializer_class(instance=instance, data=data, **kwargs)

    class Meta:
        # The name of the field to use as a discriminator. Used in
        # deserialisation and in serialisation when `class_map` is not set.
        discriminator_field = "type"
        # Maps the values of the discriminator field to a subserializer.
        discriminator_map = None
        # Maps the possible classes of the instance to a serializer. Used for
        # serialisation. If not set, it's assumed that the instance has the
        # discriminator field.
        class_map = None
