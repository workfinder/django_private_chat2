from .conf import settings


def get_user_id(user):
    return str(getattr(user, settings.CHAT_USER_ID_FIELD))
