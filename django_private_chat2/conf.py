from appconf import AppConf
from django.conf import settings  # noqa: F401


class ChatAppConf(AppConf):
    # The field to use as a FK for users in the chat system.
    USER_ID_FIELD = "pk"

    class Meta:
        prefix = "chat"
