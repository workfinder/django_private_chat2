from django.urls import path

from .consumers import ChatConsumer
from .routers import OptionalSlashRouter
from .viewsets import DialogViewSet, MessageViewSet

router = OptionalSlashRouter()

websocket_urlpatterns = [
    path("ws/chat/", ChatConsumer.as_asgi()),
]

router.register("messages", MessageViewSet, basename="messages")
router.register("dialogs", DialogViewSet, basename="dialogs")
urlpatterns = router.urls
