import bleach
from django.db import models


class HTMLField(models.TextField):

    SAFE_TAGS = ["b", "br", "i", "p", "strong", "emphasis", "li", "ul", "ol"]

    def __init__(self, tags=SAFE_TAGS, **kwargs):
        self.tags = tags
        super().__init__(**kwargs)

    def get_prep_value(self, value):
        return bleach.clean(value, tags=self.tags)

    def to_python(self, value):
        return bleach.clean(value, tags=self.tags)
