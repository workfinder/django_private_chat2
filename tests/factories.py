from django.conf import settings
from django.contrib.auth.models import User
from factory import Iterator, LazyAttribute, Sequence, SubFactory
from factory.django import DjangoModelFactory
from faker import Factory as FakerFactory
from pytz import timezone

from django_private_chat2.models import Dialog, Message

faker = FakerFactory.create()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    password = LazyAttribute(lambda _: faker.text(max_nb_chars=128))
    last_login = LazyAttribute(
        lambda x: faker.date_time_between(
            start_date="-1y", end_date="now", tzinfo=timezone(settings.TIME_ZONE)
        )
    )
    is_superuser = Iterator([True, False])
    username = Sequence(lambda n: "user_%03d" % n)
    first_name = LazyAttribute(lambda _: faker.text(max_nb_chars=150))
    last_name = LazyAttribute(lambda _: faker.text(max_nb_chars=150))
    email = faker.email()
    is_staff = Iterator([True, False])
    is_active = Iterator([True, False])
    date_joined = LazyAttribute(
        lambda x: faker.date_time_between(
            start_date="-1y", end_date="now", tzinfo=timezone(settings.TIME_ZONE)
        )
    )


class DialogFactory(DjangoModelFactory):
    class Meta:
        model = Dialog

    user1 = SubFactory(UserFactory)
    user2 = SubFactory(UserFactory)


class MessageFactory(DjangoModelFactory):
    class Meta:
        model = Message

    # is_removed = Iterator([True, False])
    sender = Iterator(User.objects.all())
    recipient = Iterator(User.objects.all())
    text = LazyAttribute(
        lambda x: faker.paragraph(nb_sentences=3, variable_nb_sentences=True)
    )
    file = LazyAttribute(lambda x: None)
    read = Iterator([True, False])
