from django.urls import include, re_path

urlpatterns = [
    re_path(
        r"", include("django_private_chat2.urls", namespace="django_private_chat2")
    ),
]
