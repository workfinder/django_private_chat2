from django.test import TestCase

from django_private_chat2.models import Dialog, Message, UploadedFile
from django_private_chat2.serializers import serialize_file_model

from .factories import UserFactory


class SerializerTests(TestCase):
    def setUp(self):
        self.sender = UserFactory.create()
        self.recipient = UserFactory.create()
        self.message = Message.objects.create(
            sender=self.sender, recipient=self.recipient, text="testText", read=True
        )
        self.dialog = Dialog.objects.filter(
            user1=self.sender, user2=self.recipient
        ).first()
        self.file = UploadedFile.objects.create(uploaded_by=self.sender, file="LICENSE")

    def test_serialize_file_model(self):
        serialized = serialize_file_model(self.file)
        o = {
            "id": str(self.file.id),
            "url": self.file.file.url,
            "size": self.file.file.size,
            "name": "LICENSE",
        }
        self.assertEqual(serialized, o)

    def tearDown(self):
        pass
